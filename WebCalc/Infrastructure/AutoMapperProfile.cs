﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCalc.Dto;
using WebCalc.Models;

namespace WebCalc.Infrastructure
{
    public class AutoMapperProfile : AutoMapper.Profile
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="AutoMapperProfile" /> class.
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<CalculationResult, CalculatorResponseDto>();
            CreateMap<Calculation, LoggedCalculationDto>();
        }
    }
}
