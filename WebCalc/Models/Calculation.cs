﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebCalc.Models
{
    public class Calculation
    {
        [Key]
        public int ID { get; set; }
        public string Expression { get; set; }

        public string IPAddress { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
