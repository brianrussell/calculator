﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCalc.Models
{
    public class CalculationResult
    {
        public string Result { get; set; }
        public bool Error { get; set; }
    }
}
