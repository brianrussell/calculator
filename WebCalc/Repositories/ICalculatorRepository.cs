﻿using System.Collections.Generic;
using WebCalc.Models;

namespace WebCalc.Repositories
{
    public interface ICalculatorRepository
    {
        IEnumerable<Calculation> GetAll();
        void Insert(Calculation calculation);
    }
}