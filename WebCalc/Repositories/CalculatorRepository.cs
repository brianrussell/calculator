﻿using System.Collections.Generic;
using System.Linq;
using WebCalc.Models;

namespace WebCalc.Repositories
{
    public class CalculatorRepository : ICalculatorRepository
    {
        private CalculatorDbContext context;

        public CalculatorRepository(CalculatorDbContext context)
        {
            this.context = context;
        }

        public void Insert(Calculation calculation)
        {
            context.Add(calculation);
            context.SaveChanges();
        }

        public IEnumerable<Calculation> GetAll()
        {
            return context.Calculations.ToList();
        }
    }
}
