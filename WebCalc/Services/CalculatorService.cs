﻿using Microsoft.AspNetCore.Http;
using NCalc;
using System;
using System.Collections.Generic;
using WebCalc.Models;
using WebCalc.Repositories;

namespace WebCalc.Services
{
    public class CalculatorService : ICalculatorService
    {
        private ICalculatorRepository calculatorRepository;
        private IHttpContextAccessor httpContextAccessor;

        public CalculatorService(ICalculatorRepository calculatorRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.calculatorRepository = calculatorRepository;
            this.httpContextAccessor = httpContextAccessor;
        }

        public CalculationResult Evaluate(string expression)
        {
            CalculationResult result = new CalculationResult();
            result.Error = false;

            try
            {
                var expr = new Expression(expression);

                var outcome = expr.Evaluate();
                result.Result = Convert.ToString(outcome);
            }
            catch ( Exception )
            {
                result.Result = string.Empty;
                result.Error = true;
            }

            LogCalculation(expression);
            return result;
        }

        public IEnumerable<Calculation> GetSavedCalculations()
        {
            return calculatorRepository.GetAll();
        }

        private void LogCalculation( string expression )
        {
            var ipAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            Calculation calculation = new Calculation();
            calculation.IPAddress = ipAddress;
            calculation.RequestDate = DateTime.Now;
            calculation.Expression = expression;
            calculatorRepository.Insert(calculation);
        }
    }
}
