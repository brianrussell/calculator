﻿using System.Collections.Generic;
using WebCalc.Models;

namespace WebCalc.Services
{
    public interface ICalculatorService
    {
        CalculationResult Evaluate(string expression);
        IEnumerable<Calculation> GetSavedCalculations();
    }
}