﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using WebCalc.Dto;
using WebCalc.Services;

namespace WebCalc.Controllers
{
    [ApiController]
    [Route("calculations")]
    public class CalculatorController : ControllerBase
    {
        private readonly ILogger<CalculatorController> logger;
        private IMapper mapper;
        private ICalculatorService calculatorService;

        public CalculatorController(ILogger<CalculatorController> logger, IMapper mapper, ICalculatorService calculatorService )
        {
            this.logger = logger;
            this.calculatorService = calculatorService;
            this.mapper = mapper;
        }

        [HttpPost]
        [Route("")]
        public ActionResult<CalculatorResponseDto> Calculate( CalculatorRequestDto request )
        {
            var result = calculatorService.Evaluate(request.Expression);

            var dto = this.mapper.Map<CalculatorResponseDto>(result);
            return Ok(dto);
        }


        [HttpGet]
        [Route("")]
        public ActionResult<IEnumerable<LoggedCalculationDto>> GetLoggedCalculations()
        {
            var data = this.calculatorService.GetSavedCalculations();
            var dtos = this.mapper.Map<IEnumerable<LoggedCalculationDto>>(data);
            return Ok(dtos);
        }
    }
}
