﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCalc.Dto
{
    public class CalculatorRequestDto
    {
        public string Expression { get; set; }
    }
}
