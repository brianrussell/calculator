﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCalc.Dto
{
    public class LoggedCalculationDto
    {
        public int ID { get; set; }

        public string Expression { get; set; }

        public string IPAddress { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
