(function() {
  'use strict';

  angular.module('app').component('calculator', {
    controller: CalculatorController,
    controllerAs: 'vm',
    templateUrl: 'app/calculator/calculator.view.html',
  });

  /** @ngInject */
  function CalculatorController($log, calculatorService) {
    const vm = this;

    vm.buffer = '';
    vm.error = false;
    // Scope variables go here:
    // vm.variable = 'value';

    vm.onButtonClick = onButtonClick;

    initialize();

    function initialize() {
      $log.debug('calculator activated');
      clear();
    }

    function onButtonClick( vtype, value ) {

      if ( vm.error ) {
        clear();
      }

      if ( vtype === 'input' || vtype==='operator' ) {
        vm.buffer += value;
      }
      else if ( vtype === 'fn') {

        if ( value === 'clr') {
          // Clear buffer command
          clear();
        }
        else if ( value === 'del') {
          // Delete last character command
          vm.buffer = vm.buffer.slice(0,-1);
        }
        else if ( value === 'eql') {
          // Evaluate input expression command
          if ( vm.buffer.length > 0)
            evaluate();
        }
      }
      $log.debug(vm.buffer);
    }

    function evaluate() {
      const dto = {
        expression: vm.buffer
      };

      calculatorService.evaluate( dto ).then(
         function (rsp) {
           if ( rsp.data.error === true ) {
             vm.error = true;
             vm.buffer = "Error";
           }
           else {
             vm.error = false;
             vm.buffer = rsp.data.result;
           }
         },
         function (err) {
           vm.buffer = "Error";
           vm.error = true;
           $log.debug( err );
         }
      );
    }

    function clear() {
      vm.buffer = '';
      vm.error = false;
    }
  }

})();
