(function() {
   'use strict';

   angular.module('app').factory('calculatorService', calculatorService);

   calculatorService.$inject = ['$http'];

   function calculatorService($http) {

      const config = {
         withCredentials: false,
      };

      const service = {
         evaluate: evaluate,
      };

      return service;

      function evaluate(dto) {
         const url = 'http://localhost:60285/calculations';
         return $http.post(url, dto, config);
      }
   }
}());