INSTRUCTIONS
===============================================================================

First, clone the repository:
git clone https://brianrussell@bitbucket.org/brianrussell/calculator.git

The solution is split into two separate projects in two separate folder hierarchies. Ideally, it would have been preferable to have everything in a single solution but that wasn't possible in a short time. That would be an exercise for a later date.

Postman
===============================================================================

I've included a Postman collection that can be imported into Postman if you have it available. 
It is in the root folder:
Calculator.postman_collection.json

It contains two requests, one to call the calulator api and a second that will return the contents of the database.


AngularJS Project
===============================================================================

The AngularJS project is in folder (root)\calc

The AngularJS project is based off a boilerplate I found (https://www.npmjs.com/package/angular-gulp-boilerplate) which seems quite good, albeit a little dated. 

SETUP
---------------------------------------

1) Assume you have NodeJS installed. If not you will need to install (see https://nodejs.org/en/).

2) After setting up Node.js you can use npm (Yarn is an alternative) to install Bower and Gulp globally:
npm install -g bower gulp-cli

3) Navigate into the project folder and install dependencies from npm:
cd calc
npm install

4) Next, install dependencies from Bower registry:
bower install


USE
---------------------------------------

1) Execute gulp serving task:

gulp serve

2) Your default browser will be launched at http://localhost:3000 serving project
http://localhost:3000


Visual Studio Solution
===============================================================================

The Visual Studio solution file is in the root folder. The project itself is in (root)\WebCalc

Implementation is ASP.NET Core 3.1. The database is EF in-memory database.
Load the solution and press F5 to run. 
That's it.

